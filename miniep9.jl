using Test

function multiplica(a, b)
    dima = size(a)
    dimb = size(b)
	if dima[2] != dimb[1]
	  return -1
	end
    c = zeros(dima[1], dimb[2])
    for i in 1:dima[1] 
        for j in 1:dimb[2]
            for k in 1:dima[2]
                c[i, j] = c[i, j] + a[i, k] * b[k, j]
            end
        end
    end
  return c
end

function matrix_pot(M, p)
     res = M
     aux = M
     if p < 1
        return "p deve ser >= 1"
     else
        for i in 1:p - 1
            res = multiplica(M, aux)
            aux = multiplica(M, aux)
        end
     end
    return res
end
	
function matrix_pot_by_squaring(M, p)
    if p < 0  
	        return matrix_pot_by_squaring(1 / M, -p)
    elseif p == 0
		return  1
    elseif p == 1
	        return  M
    elseif p % 2 == 0 
		return matrix_pot_by_squaring(M * M,  p / 2)
    elseif p % 2 == 1
		return M * matrix_pot_by_squaring(M * M, (p - 1) / 2)
    end
end

using LinearAlgebra
function compare_times()
M = Matrix(LinearAlgebra.I, 30, 30)
@time matrix_pot(M, 10)
@time matrix_pot_by_squaring(M, 10)
end
compare_times()

function test()
    @test matrix_pot([1 2 ; 3 4], 1) == [1 2 ; 3 4]
	@test matrix_pot([1 2 ; 3 4], 2) == [7 10 ; 15 22]
	@test matrix_pot([1 2 ; 3 4], 2) == [7 10 ; 15 22]
	@test matrix_pot_by_squaring([1 2 ; 3 4], 2) == [7 10 ; 15 22]
    @test matrix_pot([1 2 ; 3 4], 1) == matrix_pot_by_squaring([1 2 ; 3 4], 1)
	@test matrix_pot([1 2 ; 3 4], 2) == matrix_pot_by_squaring([1 2 ; 3 4], 2)
	@test matrix_pot([1 2 ; 3 4], 8) == matrix_pot_by_squaring([1 2 ; 3 4], 8)
	@test matrix_pot([19 32 ; 48 52], 4) == matrix_pot_by_squaring([19 32 ; 48 52], 4)
	@test matrix_pot([0 0 ; 1 0], 6) == matrix_pot_by_squaring([0 0 ; 1 0], 6)
	@test matrix_pot([4 8 0 4 ; 8 4 9 6; 9 6 4 0; 9 5 4 7], 7) == matrix_pot([4 8 0 4 ; 8 4 9 6; 9 6 4 0; 9 5 4 7], 7)
end

println(test())